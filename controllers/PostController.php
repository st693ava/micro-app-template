<?php

namespace micro\controllers;

use yii\rest\ActiveController;
use yii\web\Request;
use Yii;
use micro\models\Post;

class PostController extends ActiveController
{
    public $modelClass = 'micro\models\Post';

    public function behaviors()
    {
        // remove rateLimiter which requires an authenticated user to work
        $behaviors = parent::behaviors();
        unset($behaviors['rateLimiter']);
        return $behaviors;
    }

    public function actionTeste()
    {
        //$query= Post::find();
        //$query->select(["id","title",'body']);
        ////$query->limit=3;
        //$query->where(["id" => ["12","13","14","15"]]);
        //$query->orderBy("id ASC");
        //$query->createCommand();


        $query = Post::find()->select("id,title,body")->where(["id"=>[12,13,14,15]])->andWhere(["title"=>"!= q123"]);


        $query->all();
        return $query->sql;

        return Yii::$app->components;
        //return Yii::$app->db->getTableSchema("post");
        //return Yii::$app->db->getTableSchema("post");
        return ["userAgente"=>Yii::$app->request->userAgent];



    }
}