<?php

namespace app\modules\rest;

/**
 * api module definition class
 */
class api extends \yii\base\Module
{
    public $allowedIPs;
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'micro\modules\rest\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
