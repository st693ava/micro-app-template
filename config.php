<?php
$config= [
    'id' => 'micro-app',
    // the basePath of the application will be the `micro-app` directory
    'basePath' => __DIR__,
    // this is where the application will find all controllers
    'controllerNamespace' => 'micro\controllers',
    'language'=>'pt',
    // set an alias to enable autoloading of classes from the 'micro' namespace
    'aliases' => [
        '@micro' => __DIR__,
    ],


    'modules' => [
        'api' => [
            'allowedIPs' => ['127.0.0.1', '::1','*'],
            'class' => 'app\modules\rest\api',
        ],
    ],
    
    'components' => [

        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'sqlite:@micro/database.sqlite',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',                 	
                    'pluralize' => false,
                    'controller' => ['post']
                ],
            ],
        ],

    ],
];




return $config;