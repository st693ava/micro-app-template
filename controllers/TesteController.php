<?php

namespace micro\controllers;

use yii\web\Controller;

class TesteController extends Controller
{
    public function actionIndex()
    {
        return 'Teste!';
    }
}